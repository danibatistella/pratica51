package utfpr.ct.dainf.if62c.pratica;

import java.util.Arrays;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica51 {
    public static void main(String[] args) {

   Matriz m1 = new Matriz(3, 2);
        double[][] m = m1.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;
        
          
        Matriz transp = m1.getTransposta();
        Matriz soma = m1.soma();
        Matriz prod = m1.prod();
        
        System.out.println("Matriz original m1: " + m1);
        
        System.out.println("Matriz original m2: " + m1);
         
        System.out.println("Matriz transposta: " + transp);
        
        System.out.println("soma : " + soma);
         
        System.out.println("prod : " + prod);
         
    }
}

